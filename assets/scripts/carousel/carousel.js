var $ = require('jquery');

function Navigator(images){
    var self = this;
    this.current_image = $('div.carousel-inner div.active');
    this.images = images;
    this.left_button = $('.left.carousel-control');
    this.right_button = $('.right.carousel-control');
    self.left();
    self.right();
}

function Selector(stacks){
    var self = this;
    this.stacks = stacks;
    this.current_stack = '';
    this.current_title = '';

    self.default();
    self.select();
}

// moves the carousel left
Navigator.prototype.left = function(){
    var self = this;

    self.left_button.click(function(){
        self.images.each(function(i){
            if(i>0 && $(this).children('img').attr('src') == self.current_image.children('img').attr('src')){
                self.current_image.removeClass('active');
                self.current_image = $(self.images[i-1]);
                self.current_image.addClass('active');
                return false;
            }
        });
    });
};

// moves the carousel right
Navigator.prototype.right = function(){
    var self = this;
    var max = self.images.length;

    self.right_button.click(function(){
        self.images.each(function(i){
            if(i<max-1 && $(this).children('img').attr('src') == self.current_image.children('img').attr('src')){
                self.current_image.removeClass('active');
                self.current_image = $(self.images[i+1]);
                self.current_image.addClass('active');
                return false;
            }
        });
    });
};

// activates the first stack as default
Selector.prototype.default = function(){
    var self = this;
    self.current_stack = $('.stack').first().children('ul').children('li');
    self.current_title = $('.stack').first().children('h2').text();
    self.apply();
};

// activates the clicked stack
Selector.prototype.select = function(){
    var self = this;
    self.stacks.click(function(){
        self.current_stack = $(this).children('ul').children('li');
        self.current_title = $(this).children('h2').text();
        self.apply();
    });
};

// inserts the current stack into the html, and applies the new
Selector.prototype.apply = function(){
    var self = this;
    var stack = self.current_stack;
    var title = self.current_title;

    $('div.carousel-items').empty();
    var i=0;
    stack.each(function(){
        var image = $('<img/>').attr('src', $(this).find('img').attr('src'));
        var div_item = $('<div id="div'+i+'" class="item"/>');
        if(i===0) {
            div_item.addClass('active');
        }
        $('div.carousel-items').append(div_item);
        div_item.append(image);
        i += 1;
    });
    $('div.carousel-caption').text(title);
    $('h3.stack-title').text(title);

    var images = $('div.carousel-inner div.item');
    if (images.length > 0) {
        var navigator = new Navigator(images);
    }
};

$(document).ready(function () {
    var stacks = $('.stack');
    if (stacks.length > 0) {
        var selector = new Selector(stacks);
    }
});
