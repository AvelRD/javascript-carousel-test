<?php

namespace TweedeGolf\MainBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use TweedeGolf\MainBundle\DataFixtures\Faker\Provider\UploadedImage;
use TweedeGolf\MainBundle\Entity\Image;
use TweedeGolf\MainBundle\Entity\Stack;

class LoadChartData extends AbstractFixture implements OrderedFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritdoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create();
        $faker->addProvider(new UploadedImage($faker, $this->container));

        for ($i = 0; $i < 10; $i += 1) {
            $stack = new Stack();
            $stack->setName($faker->sentence());
            $stack->setCreatedAt($faker->dateTimeBetween('-1 year', '-1 month'));
            $stack->setUpdatedAt($faker->dateTimeBetween('-1 month', 'now'));
            $manager->persist($stack);

            for ($j = 0; $j < $faker->numberBetween(1, 5); $j += 1) {
                $image = new Image($stack);
                $createdDate = $faker->dateTimeBetween('-1 year', 'now');

                $image->setFile($faker->uploadedImage(
                    $faker->numberBetween(300, 600),
                    $faker->numberBetween(300, 600),
                    'cats'
                ));
                $image->setCreatedAt($createdDate);
                $image->setUpdatedAt($createdDate);

                $manager->persist($image);
                $stack->addImage($image);
            }
        }
        $manager->flush();
    }

    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 150;
    }
}
